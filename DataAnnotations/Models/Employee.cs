﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace DataAnnotations.Models
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("Employee Name")]
        [Required(ErrorMessage = "Nhập tên vào")]
        [StringLength(35, MinimumLength = 4)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Nhớ nhập số nhà nha")]
        [StringLength(300)]
        public string address { get; set; }

        [Required(ErrorMessage = "Lương thấp hay cao thì cũng phải nhập vào đi")]
        [Range(3000,100000000,ErrorMessage ="Salary must be between 3000 and 100000000")]
        public decimal Salary { get; set; }

        [Required(ErrorMessage = "Nhập email vào đi bạn")]
        [DataType(DataType.EmailAddress)]
        [MaxLength(50)]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}",ErrorMessage ="Nhập đúng cái email hợp lệ vào đi")]
        public string Email { get; set; }

    }
}